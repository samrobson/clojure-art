(ns genart.dynamic
  (:require [quil.core :as q])
  (:require [genart.algorithms.soundwaves :as algo]))

(def size 800)

(defn setup []
  (q/smooth))

(defn draw []
  (q/no-loop)
  (algo/draw size))