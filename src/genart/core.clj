(ns genart.core
  (:require [quil.core :as q])
  (:require [genart.dynamic :as dynamic]))

(q/defsketch example
             :title "Experiment"
             :setup dynamic/setup
             :draw dynamic/draw
             :size [800 800])