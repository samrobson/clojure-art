(ns genart.algorithms.soundwaves
  (:require [quil.core :as q]))

(def backgrounds [[51 109 136]
              [20 27 56]
              [231 156 194]])


(defn draw_curve [size window padding x]
(let [centre (/ size 2)
      r (rand 255)
      g (rand 255)
      b (rand 255)
      ystart (- centre (rand window))
      yend (+ centre (rand window))
      ystartcurve (+ ystart (rand (- centre ystart)))
      yendcurve (- yend (rand (- yend centre)))
      xstartcurve (- x (rand padding))
      xendcurve (+ x (rand padding))
      ]
  (q/stroke-weight 1)
  (q/stroke r g b)
  (if (and (>= x (/ size padding)) (<= x (- size (/ size padding))))
    (q/curve
      x ystart
      xstartcurve ystartcurve
      xendcurve yendcurve
      x yend)
    (q/point x (/ size 2))
  )
))

(defn draw [size]
  (let [[r g b] (backgrounds (rand-int 2))]
  (q/background r g b)
  (q/begin-shape)
  (doseq [i (range size)]
    (let [window (/ size 6)]
    (draw_curve size window 10 i)))
  (q/end-shape)
  (q/save "soundwaves.png")))
