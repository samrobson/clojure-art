
;; watercolour
;; recursive polygon deformation algorithm
;; for each line a -> c, find midpoint b. from a gausiaan distribution centred on B, pick new point B`
; update polygon, replacing A-> C with two lines, A -> B` and B` -> C

(ns genart.algorithms.watercolour
  (:require [quil.core :as q]))

(def backgrounds [[51 109 136]
                  [20 27 56]
                  [231 156 194]])

(defn draw_shape [s]
  ((q/stroke-weight 1)
   (q/stroke 255 255 255)
   (q/rect s s s s)))

(defn draw [size]
  (let [[r g b] (backgrounds (rand-int 2))]
    (q/background r g b)
    (q/begin-shape)
    (draw_shape 300)
    (q/end-shape)
    (q/save "watercolour.png")))
