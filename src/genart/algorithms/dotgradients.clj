(ns genart.algorithms.dotgradients
  (:require [quil.core :as q]))

(def pixelsperrow 10000)

(defn color [row, red, diff]
  (let [green 125 blue 255]
    ( list red (+ green (* diff row)) (- blue (* diff row)))))

(defn getrowvals [size rows row]
  (let [rowsize (/ size rows)] ( list (rand size) (+ (* rowsize row) (rand rowsize)))))


(defn draw [size rows]
  (q/background 255 240 240)
  (q/begin-shape)
  (q/blend-mode :blend )
  (doseq [i (range rows) j (range pixelsperrow)]
    (let [[r g b] (color i 90 3)
          [x y] (getrowvals size rows i)]
      (q/stroke-weight 1)
      (q/stroke r g b)
      (q/point x y)))
  (q/end-shape)
  (q/save "dotgradients.png"))
