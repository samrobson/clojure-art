; set grid to hold a bunch of angles

(ns genart.algorithms.flowfields
   (:require [quil.core :as q]))

(def backgrounds [[51 109 136]
                  [20 27 56]
                  [231 156 194]])

(def rows 20)

(defn set_angle [x y]
  (* (/ x rows) (q/PI)))

(defn write [x y]
  (rand 5))

(def grid (vec2d 800 800 (rand 5))

(defn vec2d
  "Return an x by y vector with all entries equal to val."
  [x y val]
  (vec (repeat y (vec (repeat x val)))))

(defn draw_grid [grid x y]
  (q/stroke-weight 1)
  (case (get grid x y)
    1 (q/stroke 0 0 0)
    2 (q/stroke 255 255 0)
    3 (q/stroke 255 0 255)
    4 (q/stroke 0 255 255)
    )
  (q/point x y))


(defn draw [size]
  (let [[r g b] (backgrounds (rand-int 2))
        rows (/ size 5)
        grid grid]
    (q/background r g b)
    (q/begin-shape))
    (doseq [x (range size)
            y (range size)]
      (draw_grid grid x y)
    (q/end-shape)
    (q/save "soundwaves.png")))